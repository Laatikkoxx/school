import pygame

pygame.init()

width = 640
height = 480

naytto = pygame.display.set_mode((width, height))
naytto.fill((53, 53, 53))

#Lisätään robotti ja otetaan sen koko
robo = pygame.image.load("robo.png")
robowidth = robo.get_width()
roboheight = robo.get_height()

#Robottien lisääminen
naytto.blit(robo, (0, 0))
naytto.blit(robo, (width - robowidth , 0))

naytto.blit(robo, (0, height - roboheight))
naytto.blit(robo, (width - robowidth, height - roboheight))

pygame.display.flip()

while True:
    for tapahtuma in pygame.event.get():
        if tapahtuma.type == pygame.QUIT:
            exit()