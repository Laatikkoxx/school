import pygame

pygame.init()
naytto = pygame.display.set_mode((640, 480))

robo = pygame.image.load("robo.png")
robowidth = robo.get_width()
roboheight = robo.get_height()

x = 0
y = 0

x2 = 0
y2 = 150
xnopeus = 0.5
x2nopeus = 1
kello = pygame.time.Clock()

naytto.fill((0, 0, 0)) # tausta
pygame.display.flip()

while True:

    for tapahtuma in pygame.event.get():
        if tapahtuma.type == pygame.QUIT:
            exit()

    pygame.display.flip()
    naytto.blit(robo, (x, y))
    naytto.blit(robo, (x2, y2))
    x += xnopeus
    x2 += x2nopeus 

    if xnopeus > 0 and x + robowidth >= 640:
        xnopeus = -xnopeus
    if xnopeus < 0 and x <= 0:
        xnopeus = -xnopeus

    if x2nopeus > 0 and x2 + robowidth >= 640:
        x2nopeus = -x2nopeus
    if x2nopeus < 0 and x2 <= 0:
        x2nopeus = -x2nopeus 

    kello.tick(800)