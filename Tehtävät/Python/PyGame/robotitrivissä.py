import pygame

pygame.init()

width = 640
height = 480

naytto = pygame.display.set_mode((width, height))
naytto.fill((33, 33, 33))

#Lisätään robotti ja otetaan sen koko
robo = pygame.image.load("robo.png")
robowidth = robo.get_width()
roboheight = robo.get_height()

#Robottien lisääminen
for x in range(1, 11):
    naytto.blit(robo, (robowidth * x , roboheight ))



pygame.display.flip()

while True:
    for tapahtuma in pygame.event.get():
        if tapahtuma.type == pygame.QUIT:
            exit()