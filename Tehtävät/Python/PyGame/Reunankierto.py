import pygame

pygame.init()
naytto = pygame.display.set_mode((640, 480))

robo = pygame.image.load("robo.png")
robowidth = robo.get_width()
roboheight = robo.get_height()

x = 0
y = 0
xnopeus = 1
ynopeus = 0
kello = pygame.time.Clock()

naytto.fill((0, 0, 0)) # tausta
pygame.display.flip()

while True:

    for tapahtuma in pygame.event.get():
        if tapahtuma.type == pygame.QUIT:
            exit()

    pygame.display.flip()
    naytto.blit(robo, (x, y))
    x += xnopeus
    y += ynopeus

    if xnopeus > 0 and x + robowidth >= 640:
        xnopeus = 0
        ynopeus = 1
    elif ynopeus > 0 and y + roboheight  >= 480:
        xnopeus = -1
        ynopeus = 0
    elif xnopeus < 0 and x <= 0:
        xnopeus = 0
        ynopeus = -1
    elif xnopeus == 0 and y <= 0:
        xnopeus = 1
        ynopeus = 0

   






    kello.tick(800)