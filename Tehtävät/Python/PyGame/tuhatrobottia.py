import pygame
import random

pygame.init()

width = 640
height = 480

naytto = pygame.display.set_mode((width, height))
naytto.fill((33, 33, 33))

#Lisätään robotti ja otetaan sen koko
robo = pygame.image.load("robo.png")
robowidth = robo.get_width() 
roboheight = robo.get_height()

#Robottien lisääminen
rivi = 0
for y in range(1000):
    #random sijainti ruudun sisäll'
    x = random.randint(0, width - robowidth)
    y = random.randint(0, height - roboheight)
    naytto.blit(robo, (x, y))


pygame.display.flip()

while True:
    for tapahtuma in pygame.event.get():
        if tapahtuma.type == pygame.QUIT:
            exit()