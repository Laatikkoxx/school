const city = "Helsinki"
const settings = {
    "async": true,
    "crossDomain": true,
    "url": `https://goweather.herokuapp.com/weather/${city}`,
    "method": "GET",
    "headers": {
        "user-agent": "vscode-restclient"
      }
    }

function getVal() {
    const city = document.getElementById("Find").value;
    console.log(city);
    $.ajax(settings).done(function (response) {
        document.getElementById("city").textContent=city;
        document.getElementById("temperature").textContent=response.forecast;
        console.log(response);
      });
  }
