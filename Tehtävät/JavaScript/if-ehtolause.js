const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });
  
  readline.question('Lämpötila? ', temperature => {
    if (temperature >= 20){
        console.log(`Lämmintä!`);
        readline.close();
    }
    else if (temperature < -1) {
        console.log(`Pakkasta!`);
        readline.close();
    }
    else if (temperature < 20 && temperature >= 0){
        console.log(`Kylmähän siellä on`);
        readline.close();
    }
    else{
        console.log(`Syötä numero!`);
        readline.close();
    }
  }); 