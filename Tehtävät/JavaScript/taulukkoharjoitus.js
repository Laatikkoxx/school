const arr = [1, 2, 3, 4, 5];
const arr6 = [1, 2, 3, 4, 5];

// listaa taulukon sisällön eri riveille
for (let index = 0; index < arr.length; index++) {
	const element = arr[index];
	console.log(element);
}

// laskee laskun jokaisella numerolla
const taulukko = arr6.map(value =>{
	return value*2;
});
console.log(taulukko);

// tulostaa vain ne jotka sisältää "i"
const arr3 = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
const tulos = arr3.filter(word => word.includes("i"));
console.log(tulos);

// tulostaa listan aakkosjärjestyksessä
const tulos2 = arr3.sort();
console.log(tulos2);

// poistaa taulukon ensimmäisen arvon eli banaanin
const arr4 = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
arr4.shift()
console.log(arr4);

// lisää sipulin taulukkoon
const sipuli = arr4.push('sipuli');
console.log(arr4)
